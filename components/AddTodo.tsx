import { FormEvent, useContext, useState } from 'react';
import { ITodo, TodoContext } from './context/todoContext';

const AddTodo: React.FC = () => {
  const { saveTodo } = useContext(TodoContext);
  const [formData, setFormData] = useState<ITodo | {}>();

  const handleForm = (e: FormEvent<HTMLInputElement>): void => {
    setFormData({
      ...formData,
      [e.currentTarget.id]: e.currentTarget.value,
    });
  };

  const handleSaveTodo = (e: FormEvent, formData: ITodo | any) => {
    e.preventDefault();
    saveTodo(formData);
  };

  return (
    <form className="Form" onSubmit={(e) => handleSaveTodo(e, formData)}>
      <div>
        <div>
          <label htmlFor="name">Título</label>
          <input onChange={handleForm} type="text" id="title" />
        </div>
        <div>
          <label htmlFor="description">Descripción</label>
          <input onChange={handleForm} type="text" id="description" />
        </div>
      </div>
      <button disabled={formData === undefined ? true : false}>
        Agregar Tarea
      </button>
    </form>
  );
};

export default AddTodo;
