import * as React from 'react';

import Todo from '../components/Todo';
import { ITodo, TodoContext } from './context/todoContext';

const Todos = () => {
  const { todos, updateTodo } = React.useContext(TodoContext);
  return (
    <>
      {todos.map((todo: ITodo, index) => (
        <Todo key={index} todo={todo} updateTodo={updateTodo} />
      ))}
    </>
  );
};

export default Todos;
