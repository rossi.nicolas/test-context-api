import { FC, FunctionComponent, useState } from 'react';
import { createContext } from 'react';

export interface ITodo {
  id: number;
  title: string;
  description: string;
  status: boolean;
}

export type ContextType = {
  todos: ITodo[];
  saveTodo: (todo: ITodo) => void;
  updateTodo: (id: number) => void;
};

const default_values: ContextType = {
  todos: [],
  saveTodo: () => {},
  updateTodo: () => {},
};
export const TodoContext = createContext<ContextType>(default_values);

const TodoProvider: React.FC = ({ children }) => {
  const [todos, setTodos] = useState<ITodo[]>([
    {
      id: 1,
      title: 'Tarea 1',
      description: 'Mi primer tarea',
      status: false,
    },
    {
      id: 2,
      title: 'tarea 2',
      description: 'Mi segunda tarea',
      status: true,
    },
  ]);

  const saveTodo = (todo: ITodo) => {
    const newTodo: ITodo = {
      id: Math.random(), // not really unique - but fine for this example
      title: todo.title,
      description: todo.description,
      status: false,
    };
    setTodos([...todos, newTodo]);
  };

  const updateTodo = (id: number) => {
    todos.filter((todo: ITodo) => {
      if (todo.id === id) {
        todo.status = true;
        setTodos([...todos]);
      }
    });
  };

  return (
    <TodoContext.Provider value={{ todos, saveTodo, updateTodo }}>
      {children}
    </TodoContext.Provider>
  );
};

export default TodoProvider;
