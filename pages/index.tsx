import Head from 'next/head';
import Image from 'next/image';
import React from 'react';
import AddTodo from '../components/AddTodo';
import TodoProvider from '../components/context/todoContext';
import Todos from '../components/Todos';

export default function Home() {
  return (
    <TodoProvider>
      <main className="App">
        <h1>Mis Tareas</h1>
        <AddTodo />
        <Todos />
      </main>
    </TodoProvider>
  );
}
